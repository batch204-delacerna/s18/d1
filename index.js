//console.log("Hello, B204!");

// name - parameter
function printName(name)
{
	console.log("My Name is " + name);
}


// Data passed into a function invocation can be received by the function
// This is what we call an "argument"
printName("Miah");

function checkDivisibilityBy8(num)
{
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(456);

// Variables can also be passed as an argument
let sampleVariable = 64;
checkDivisibilityBy8(sampleVariable);

/*
	Mini-Activity:
		1. Create a function which is able to receive data as an argument
			- This function should be able to receive the name of your favorite superhero
			- Display the name of your favorite superhero in the console.
*/

function printMyFavoriteSuperhero(nickName)
{
	console.log("My favorite superhero is " + nickName);
}

printMyFavoriteSuperhero("Batman");
printMyFavoriteSuperhero("Flash");

function otherPrintName(name)
{
	console.log("My Name is " + name);
}

otherPrintName("Mikki");
// Function calling without arguments will result to undefined parameters.
otherPrintName();

otherPrintName(["Mark","Jayson","Raf"]);

// Using Multiple Parameters
// Multiple "arguments" will correspond to the number of paramters declared in a function in succeeding order.
function createFullName(firstName, middleName, lastName)
{
	console.log(firstName + " " + middleName + " "+lastName);
}
createFullName("Mikki","Dela", "Cerna");
//"Mikki" > firstName
//"Dela" > middleName
//"Cerna" > lastName

createFullName("Mikki","Dela");
createFullName("Mikki","Dela","Mikki","Dela", "Cerna");

//Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

/*
	Mini-Activity:
		Create a function which will be able to receive 5 arguments.
			-Receive 5 of your favorite songs.
			-Display / print the passed 5 favorite songs in the console when the function is invoked.

*/

function printMyFaveSongs(song1, song2, song3, song4, song5)
{
	console.log("My 5 favorite songs are : \n" + song1 + "\n" + song2 + "\n" + song3 + "\n" + song4 + "\n" + song5);
}

printMyFaveSongs("Bawat Daan", "Ikaw lang", "Paraluman", "Tahanan", "You were beautiful");

// Return Statement
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstName, middleName, lastName)
{
	// console.log("This message wil be printed");
	return firstName + ' ' + middleName + ' ' + lastName
	console.log("Will this message be printed?");
}
let completeName = returnFullName("Jane", "Shatur", "Regina");
console.log(completeName);
console.log(returnFullName("Jane", "Shatur", "Regina"));

function returnAddress(city, country) 
{
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Manila", "Philippines");
console.log(myAddress);

// function printPlayerInfo(username, level, job)
// {
// 	console.log("Username: " + username);
// 	console.log("Level: " + level);
// 	console.log("Job: " + job);
// }

//  let user1 = printPlayerInfo("Mikki D Great", 69, "Knight");
//  console.log(user1);  // printPlayerInfo function has no return value

function printPlayerInfo(username, level, job)
{
	return "Username: " + username + "\nLevel: " + level +"\nJob: " + job;
}

 let user1 = printPlayerInfo("Mikki D Great", 69, "Knight");
 console.log(user1);  // printPlayerInfo function has no return value